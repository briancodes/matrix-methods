__author__ = 'Brahian Ocampo Uribe'

import numpy as np


class Matrix:
    '''
    This class provides the API for read and transform matrix
    for any Lineal System of Equations (LSE) expressed like:
    
        A*X = B,
    
    The matrix is transformed to:
        
        Xn = T*Xn-1 + C
    
    So, constants matrix T and C are genereated validating convergence
    to the exact value (Xe) throught the method: 
    
    1. Check if the matrix is diagonally dominant.
    2. In this case the row/equation is assign correspondiently to their variable.
    3. In the other case, variables are related to row/equation isolating the 
    variable with major coefficient by row/equation.
    '''

    COL_SEPARATOR = ','
    ROW_SEPARATOR = ';'

    ERROR_TAG = '[ERROR]: '
    LIMIT_ITERATIONS = 100

    X0 = None
    Xe = None

    A = None
    B = None

    T = None
    C = None    

    tolerance = None

    def __init__(self, X0=None, A=None, B=None, tolerance=None):
        '''
        Start a matrix from arguments or inputs. Inputs are 
        requested to the user by Python's CLI.
        '''
        if X0 and A and B and tolerance:
            self.X0 = self._column_vector('X0', X0)
            self._coefficients(A)
            self.B = self._column_vector('B', B)
            self._validate_matrix()
            self.tolerance = tolerance
            print('Tolerance = {}'.format(tolerance))
        else:
            self._input()

        # Calculate the exact value (Xe) throught inverse matrix method
        self.direct_method()
        self._convert()

    def direct_method(self):
        '''
        Inverse matrix method: Xe = A^-1 + B
        '''
        self.Xe = np.matmul(np.linalg.inv(self.A), self.B)
        self._print('\nXe', self.Xe)
    
    def is_diagonally_dominant(self, print_message=True):
        # Find diagonal coefficients
        D = np.diag(np.abs(self.A))
        # Calculate the cofficients (absolute value) sum excluding diagonal
        # coefficient
        S = np.sum(np.abs(self.A), axis=1) - D 

        # Validate if the diagonal coefficient is greater than the sum of 
        # absolute coefficient values
        is_dd = np.all(D > S)
        message = 'Convergence criterion: A is{} diagonally dominant'
        
        if print_message:
            if is_dd:
                print(message.format(''))
            else:
                print(message.format(' NOT'))
                # In case the matriz isn't diagonally dominant (i.e. this'nt convergent)
                # the user must decide to continue 
                ans = bool(input('Do you want to continue? (Type any key to continue): '))

                if not ans:
                    sys.exit('Program stopped')

        return is_dd

    def _input(self):
        '''
        Read from user inputs the initial values X0, A, B and tolerance value.
        These values are assigned to the attributes of class. 
        '''
        self.X0 = self._column_vector_input('X0')
        self._coefficients_input()
        self.B = self._column_vector_input('B')
        self._validate_matrix()
        self._tolerance_input()
    
    def _column_vector_input(self, label):
        '''
        Read a column matrix like a transposed vector (1, n) from user input
        '''
        vector = input('Enter initial values ({} transpose): '.format(label))
        return self._column_vector(label, vector)

    def _column_vector(self, label, vector):
        '''
        Transform transposed vector to the column matrix (n, 1)
        '''
        vector = np.transpose(np.array([ self._get_vector_values(vector) ]))
        self._print(label, vector)

        # Validate column vector dimenssion and shape
        if (not vector.ndim == 2) and vector.shape[1] != 1:
            raise ValueError('{} Vector ({}) must be unidimensional'.format(self.ERROR_TAG, label))

        print('\n{}: OK'.format(label))
        return vector

    def _coefficients_input(self):
        '''
        Read the coefficient values (A) of the SLE from user input
        '''
        A = input('Enter coefficients values (A): ')
        self._coefficients(A)
    
    def _coefficients(self, A):
        '''
        Transform input to the coefficient matrix
        '''
        self.A = np.array([ self._get_vector_values(x) for x in A.split(self.ROW_SEPARATOR)])
        self._print('A', self.A)

        # Validate matrix dimenssion
        if not self.A.ndim == 2:
            raise ValueError(self._error('Matrix (A) must be bidimensional'))
        
        print('\nA: OK')
    
    def _validate_matrix(self):
        # Validate shapes relation between coefficient matrix and column matrix
        if not (self.A.shape[0] == self.B.shape[0]):
            raise ValueError(self._error('A and B uncompatible,  n != m respectively'))

        print('A and B dimenssions: OK')

    def _tolerance_input(self):
        '''
        Read tolerance from user input
        '''
        self.tolerance = float(eval(input('Enter tolerance: ')))

    def _convert(self):
        '''
        Calculate constant matrix values for T and C.
        '''
        # Init arrays are zeros arrays
        T = np.zeros(self.A.shape)
        C = np.zeros(self.B.shape)

        # Validate convergence
        is_dd = self.is_diagonally_dominant()

        for index, row in enumerate(np.copy(self.A)):
            # Adjust the SLE row/equation by row/equation
            if not is_dd:
                n = self._get_available_variable_exponent(T, row)
                t, c = self._calculate_new_values(np.copy(row), n, index)
            else:
                n = index
                t, c = self._calculate_new_values(row, n, index)
            
            # Assigned values re-oredered
            T[n] = t
            C[n] = c

        self._print('T', T)
        self._print('C', C)
        self.T = T
        self.C = C

    def _calculate_new_values(self, temp_row, n, i):
        '''
        Calculate the values for T and C matrix from terms transposition
        '''
        max_coefficient = temp_row[n]
        new_row = np.copy(temp_row)
        new_row[n] = 0

        # Transpoisition of row/equation terms (isolate the variable)
        t = new_row * -1/max_coefficient
        c = self.B[i][0] * 1/max_coefficient

        return t, c

    def _get_available_variable_exponent(self, T, row):
        '''
        Get position (index/order) of the row/equation in T matrix
        corresponding to variable exponent
        '''
        b = np.copy(row)
        # Get position (index) of the max coefficient in the row/equation
        n = b.argmax()

        # Validate position (index/order) availability
        while np.any(T[n]):
            b[n] = 0
            # Get position for the next major coefficient in the row/equation
            n = b.argmax()
        
        return n

    def _get_vector_values(self, vector):
        '''
        Get list of number values from string by comma separator
        '''
        return list(map(eval, vector.split(self.COL_SEPARATOR))) 

    def _error(self, message):
        '''
        Get error format
        '''
        return '{} {}'.format(self.ERROR_TAG, message)

    def _print(self, label, object):
        '''
        Print formatted matrix values 
        '''
        print('{} = '.format(label))
        print(object)
