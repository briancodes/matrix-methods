import sys
import numpy as np

from core import Matrix


class JacobiMatrix(Matrix):
    '''
    This class allows calculate the approximations of a LSE.

    It only provides one method for its purpose.
    '''

    def calculate_approximations(self):
        '''
        For calculate approximations it's used the formule: X = T*X + C.
        The stop criteria is applied using Tolerance to absolute error.

        Aditionaly, for cases without convergence is implemented another stop
        criteria called Limit iterations with a default value for this.
        '''
        # Calculate first approximation
        X = np.add(np.matmul(self.T, self.X0), self.C)
        # Diferrence between first approximation and the seed value (X1 - X0)
        # for calculate absolute error, it'll be used in the stop criteria
        difference = np.add(X, self.X0 * -1)
        counter = 1

        self._print('X1', X)

        while not (np.linalg.norm(difference) < self.tolerance) and counter <= self.LIMIT_ITERATIONS:
            Xpre = np.copy(X)
            counter += 1

            # Next approximation... X*T 
            XT = np.matmul(self.T, Xpre)
            # X = X*T + C
            X = np.add(XT, self.C)

            self._print('X{} = '.format(counter), X)
            # Diferrence between the current approximation and the previous approximation (Xn - Xn-1)
            # for calculate absolute error, it'll be used in the stop criteria
            difference = np.add(X, Xpre * -1)
        
        if counter > self.LIMIT_ITERATIONS:
            print('\nLimit iterations reached!')
        
        print('\nProgram has finished succesfully.')