# Number methods for solve matrix #

This code represents number methods for solve matrix problems like Jacabi, Gauss-Seidel initially.

## How do I get set up? ##

First of all, it's neccesary download and install python **v3.8**, you can look for your system version here: [Python's downloads ](https://www.python.org/downloads/)

Then, you need setup **Pip3** as Python's dependency manager, you can check the official documentation of pip here: [Pip installing](https://pip.pypa.io/en/stable/installing/)

### Download the program ###

Simple, you only have to clone this repo (this is code is *open source*, and the this is public git repository)

    git clone https://brian7@bitbucket.org/briancodes/matrix-methods.git 

Late, just locate wherever you have downloaded the code:

    cd <your-download-location>/matrix-methods

### Dependencies ###

All of the matrix operations are made with **Numpy** python's library, so it's the main dependency, to install all program's dependencies, just type:

    pip install -r requirements.txt

For run the program, type:

    python setup.py

In this file you will find commented examples problems, you can uncomment those lines to execute pre-defined cases.

## Who do I talk to? ##

* Repo owner or admin: brahian_ocampo82161@elpoli.edu.co
* Other community or team contact

### License ###

MIT License

Copyright (c) 2020 Brahian Ocampo Uribe

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.