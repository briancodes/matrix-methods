from jacobi import JacobiMatrix

ans = input('Execute program (kind Enter), run example (kind any key and enter): ')

if not bool(ans):
    ''' Simple '''

    m = JacobiMatrix()
    m.calculate_approximations()
else:
    doc = '''
    Class example (Converge)
    '''
    print(doc)
    X0 = '0,0,0,0'
    A  = '10,-1,2,0;-1,11,-1,3;2,-1,10,-1;0,3,-1,8'
    B  = '6,25,-11,15'
    tolerance = 0.01

    m = JacobiMatrix(X0=X0, A=A, B=B, tolerance=tolerance)
    m.calculate_approximations()

    doc = '''
    Class example (Not converge)
    '''
    # X0 = '0,0,0,0'
    # A = '-1,3,5,2;1,9,8,4;0,1,0,1;2,1,1,-1'
    # B = '10,15,2,-3'
    # tolerance = 0.01

    # m = JacobiMatrix(X0=X0, A=A, B=B, tolerance=tolerance)
    # m.calculate_approximations()

    doc = '''
    Ejercicio 1
    '''
    # X0 = '0,0,0'
    # A  = '0.55, 0.2, 0.25; 0.3, 0.5, 0.2; 0.15, 0.3, 0.55'
    # B  = '4800, 5800, 5700'
    # tolerance = 0.01

    # m = JacobiMatrix(X0=X0, A=A, B=B, tolerance=tolerance)
    # m.calculate_approximations()

    doc = '''
    Ejercicio 2
    '''
    # X0 = '0,0,0,0'
    # A  = '80, 15, 35, 60; 28, 72, 57, 25; 20, 20, 12, 20; 50, 10, 20, 60'
    # B  = '230, 180, 80, 160'
    # tolerance = 0.01

    # m = JacobiMatrix(X0=X0, A=A, B=B, tolerance=tolerance)
    # m.calculate_approximations()